//Author: Dan Willis Njoumene Douanla | 2033804

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application{

    private RpsGame game;

    public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 

        Button rock = new Button("rock");
        Button paper = new Button("paper");
        Button scissors = new Button("scissors");

        TextField message = new TextField("Welcome!");
        TextField wins = new TextField("wins: 0");
        TextField losses = new TextField("losses: 0");
        TextField ties = new TextField("ties: 0");
        

        HBox buttons = new HBox();
        buttons.getChildren().addAll(rock, paper, scissors);

        HBox textFields = new HBox();
        textFields.getChildren().addAll(message, wins, losses, ties);  
        message.setPrefWidth(200);

        VBox overall = new VBox();
        overall.getChildren().addAll(buttons, textFields);

        root.getChildren().addAll(overall);
        this.game = new RpsGame();
        rock.setOnAction(new RpsChoice(message, wins, losses, ties, "rock", game));
        paper.setOnAction(new RpsChoice(message, wins, losses, ties, "paper", game));
        scissors.setOnAction(new RpsChoice(message, wins, losses, ties, "scissors", game));

		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }  
}
