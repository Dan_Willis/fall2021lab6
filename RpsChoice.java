//Author: Dan Willis Njoumene Douanla | 2033804

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>{
    
    private TextField message;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String input;
    private RpsGame game;

    public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String input, RpsGame game) {
        this.message = message;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.input = input;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e){
        String text = this.game.playRound(this.input);
        this.message.setText(text);
        this.wins.setText("wins: " + this.game.getWins());
        this.ties.setText("ties: " + this.game.getTies()); 
        this.losses.setText("losses: " + this.game.getLosses());
        
    }

}
