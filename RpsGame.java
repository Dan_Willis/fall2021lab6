//Author: Dan Willis Njoumene Douanla | 2033804

import java.util.Random;

public class RpsGame{

    private int wins;
    private int ties;
    private int losses;
    private Random random;

    public RpsGame(){
        this.random = new Random();
    }

    public int getTies(){
        return this.ties;
    }

    public int getWins(){
        return this.wins;
    }

    public int getLosses(){
        return this.losses;
    }

    public String playRound(String input){
        int rock = 0, paper = 1, scissors = 2;
        int randomNumber = this.random.nextInt(3);
        input = input.toLowerCase();

        //Options if the computer got Rock
        if(randomNumber == rock){
            if(input.equals("rock")){
                this.ties++;
                return"Rock vs. Rock, Its a tie!";
            }else if(input.equals("paper")){
                this.wins++;
                return"Rock vs. Paper! You win!"; 
            }else if(input.equals("scissors")){
                this.losses++;    
                return"Rock vs. Scissors! You lose!";      
            }
        }  
        //Options if the computer got paper
        else if(randomNumber == paper){
            if(input.equals("rock")){
                this.losses++;
                return"Paper vs. Rock! You lose!";
            }else if(input.equals("scissors")){
                this.wins++;
                return"Paper vs. Scissors! You win!";
            }else if(input.equals("paper")){
                this.ties++;
                return"Paper vs. Paper! Its a tie!";
            } 
        }
        //Options if the computer got scissors
        else if(randomNumber == scissors){
            if(input.equals("rock")){
                this.wins++;
                return"Scissors vs. Rock! You win!";
            }else if(input.equals("scissors")){
                this.ties++;
                return"Scissors vs. Scissors, Its a tie!";
            }else if(input.equals("paper")){
                this.losses++;
                return"Scissors vs Paper! You lose!";
            }
        }
        return "Wrong input!";
    }

}